function apagarMensagem(mensagem){
    mensagem.parentNode.removeChild(mensagem)
}

let buttonEnviar = document.querySelector("#button_enviar")
buttonEnviar.addEventListener("click", function (){

    let input = document.querySelector("#textmsg")

    let divisao = document.createElement("div")
    let mensagem = document.createElement("p")
    let divisaoBotoes = document.createElement("div")
    let buttonEditar = document.createElement("button")
    let buttonApagar = document.createElement("button")

    divisao.classList.add("mensagem-enviada")
    mensagem.classList.add("mensagem")
    divisaoBotoes.classList.add("botoes")
    buttonEditar.classList.add("editar")
    buttonApagar.classList.add("apagar")

    mensagem.innerText = input.value
    buttonEditar.innerText = "Editar"
    buttonApagar.innerText = "Apagar"

    buttonApagar.addEventListener("click", () => apagarMensagem(divisao))

    divisaoBotoes.append(buttonEditar)
    divisaoBotoes.append(buttonApagar)
    divisao.append(mensagem)
    divisao.append(divisaoBotoes)

    let historico = document.querySelector(".display")
    historico.append(divisao)
    input.value = ""
});
